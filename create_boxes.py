from pynput import keyboard
from pynput import mouse
import json

vertical_offset = 40
box_stage = 0
image_num = 1
image = "2019-03-30 (1)"
listen_click = True
box_num = 0
data = {}

"""Output data format is Data{Image[Box[[x1, y1],[x2, y2]]]}

Use: click on the upper left-hand corner, then the bottom right-hand. F7 for another box, 
f8 for new image, f10 when done"""

def on_key(key):
    """Check keypress for box logic

    Keyword arguments:
        key -- the key that is pressed
    """
    global image_num
    global box_num
    global image
    global listen_click
    global box_stage
    if (key == keyboard.Key.f10):
        print(data)
        with open('imagedata_verification.json', 'w') as fp:
            json.dump(data, fp)
    elif (key == keyboard.Key.f8):
        if listen_click:
            print("Finish box!")
        else:
            box_stage = 0
            listen_click = True
            box_num = 0
            image_num += 1
            image = "2019-03-30 (" + str(image_num) + ")"
            print("New image.")
    elif (key == keyboard.Key.f7):
        listen_click = True
        box_num += 1

key_listener = keyboard.Listener(on_release=on_key)
key_listener.start()

def on_click(x, y, button, pressed):
    """Check mouse click for box logic

        Keyword arguments:
            x, y -- coordinates on the screen of press
            button -- the button pressed
            pressed -- control button-down -up
        """
    if pressed:
        global box_stage
        global data
        global box_num
        global listen_click
        if listen_click:
            if box_stage == 0:
                if box_num == 0:
                    data[image] = [[(x, y - vertical_offset)]]
                else:
                    data[image].append([(x, y - vertical_offset)])
                box_stage += 1
            elif box_stage == 1:
                data[image][box_num].append((x, y - vertical_offset))
                listen_click = False
                print("Box " + str(box_num) + " done.")
                box_stage = 0

with mouse.Listener(on_click=on_click) as listener:
    listener.join()

# left corner then bottom corner break loop to next object, key to next image
def box_loop():
    # box_stage = 0
    # box_coords = ()
    while True:
        pass

box_loop()