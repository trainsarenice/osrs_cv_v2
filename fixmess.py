import json

json_file = open('imagedata.json')
json_str = json_file.read()
data = json.loads(json_str)

for key in data:
    for box in range(len(data[key])):
        for i in range(len(data[key][box])):
            data[key][box][i] = [data[key][box][i][0], data[key][box][i][1] - 40]

with open('imagedata.json', 'w') as fp:
    json.dump(data, fp)