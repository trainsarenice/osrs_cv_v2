import tensorflow as tf
from pathlib import Path
import os
import sys
import json
from object_detection.utils import dataset_util

"""local-note: run with default tensorflow install, not CUDA"""

flags = tf.app.flags
flags.DEFINE_string('output_path', '', "C:\\Users\\pnkid\\PycharmProjects\\OSRS_auto")
FLAGS = flags.FLAGS

json_file = open('imagedata_verification.json')
json_str = json_file.read()
data = json.loads(json_str)
print(data)

def create_dataset(image, payload):
    height = 1040
    width = 1920
    filename = "images/" + image + ".png"
    encoded_image_data = tf.gfile.GFile(filename, 'rb').read()
    # encoded_image_data = tf.read_file(filename)
    image_format = 'png'
    filename = bytes(filename, 'utf-8')
    image_format = bytes(image_format, 'utf-8')

    xmins = []
    xmaxs = []
    ymins = []
    ymaxs = []

    classes_text = []
    classes = []

    for box in payload:
        xmins.append(box[0][0])
        xmaxs.append(box[1][0])
        ymins.append(box[0][1])
        ymaxs.append(box[1][1])
        classes_text.append(bytes("iron", 'utf-8'))
        classes.append(1)

    tf_set = tf.train.Example(features=tf.train.Features(feature={
        'image/height': dataset_util.int64_feature(height),
        'image/width': dataset_util.int64_feature(width),
        'image/filename': dataset_util.bytes_feature(filename),
        'image/source_id': dataset_util.bytes_feature(filename),
        'image/encoded': dataset_util.bytes_feature(encoded_image_data),
        'image/format': dataset_util.bytes_feature(image_format),
        'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
        'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
        'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
        'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
        'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
        'image/object/class/label': dataset_util.int64_list_feature(classes),
    }))

    return tf_set


def main(_):
    writer = tf.python_io.TFRecordWriter("data/data_verify.tf")

    for key in data:
        tf_example = create_dataset(key, data[key])
        writer.write(tf_example.SerializeToString())

    writer.close()

if __name__ == '__main__':
  tf.app.run()

